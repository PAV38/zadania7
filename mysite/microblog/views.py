# Create your views here.
from django import forms
from django.http import HttpResponse
from django.shortcuts import render
from models import Entry


class EntryForm(forms.Form):
    title = forms.CharField(max_length=50)
    entry = forms.CharField(max_length=500)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50)


def index(request):
    logged = False
    try:
        request.session['loginabc']
        request.session['logInToSite']
        logged = True
    except:
        logged = False
    list = Entry.objects.all()
    return render(request, 'microblog/index.html', {'logged': logged, 'list': list})


def added(request):
    try:
        if request.method == 'POST':
            logg = request.session['logInToSite']
            form = EntryForm(request.POST)
            print form
            if form.is_valid():
                title = form.cleaned_data['title']
                entry = form.cleaned_data['entry']
                try:
                    author = request.session['loginabc']
                except:
                    author = ''
                if author != '':
                    b = Entry(Author=author, Title=title, Text=entry)
                    b.save()
                else:
                    title = ''
                    entry = 'You must be logged in to add post'
            else:
                title = ''
                entry = 'Both fields have to be filled'
        else:
            title = ''
            entry = 'Input is invalid'
        return render(request, 'microblog/added.html', {'title': title, 'entry': entry, 'logg': logg})
    except KeyError:
        return render(request, 'microblog/error.html')

def logged(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print form
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            if (username == 'user1' and password == 'password1') or (
                        username == 'user2' and password == 'password2') or (
                        username == 'user3' and password == 'password3'):
                #tworzenie ciasteczka
                request.session['loginabc'] = username
                request.session['logInToSite'] = True
                info = 'You have succesfully logged in as ' + request.session['loginabc'] + '        Login: ' + str(request.session['logInToSite'])
            else:
                info = 'Username or password is incorrect'
        else:
            info = 'Username or password is incorrect'
    else:
        info = 'Username or password is incorrect'
    return render(request, 'microblog/logged.html', {'info': info})


def logout(request):
    try:
        del request.session['loginabc']
        request.session['logInToSite'] = False
        info = str(request.session['logInToSite'])
    except KeyError:
        pass
    return render(request, 'microblog/logout.html', {'info': info})

def Error(request):
    return render(request, 'microblog/error.html')

def LoginUserArticles(request):
    try:
        logged = request.session['logInToSite']
        list = Entry.objects.filter(Author__exact = request.session['loginabc'])
        return render(request, 'microblog/myArticles.html', {'logged': logged, 'list': list})
    except KeyError:
        list = Entry.objects.all()
        return render(request, 'microblog/error.html')
